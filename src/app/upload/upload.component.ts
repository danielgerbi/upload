import { Component, OnInit } from '@angular/core';
import { TouchSequence } from 'selenium-webdriver';
import { invalid } from '../../../node_modules/@angular/compiler/src/render3/view/util';

import {  HttpClientModule, HttpClient  } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { empty } from '../../../node_modules/rxjs';


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  private filepath = 'Browse file...';
  private file: File;
  private fileContent;
  private isImage = false;
  private isPdf = false;
  private emailAddress = "";
  private ticketId = "";
  private yesUpload = false;
private email : boolean ;
private validator = require("email-validator");
 private  selectedFile = false;
  private numberOfMinutes = -1;
cvData : string;



//still to fix how to display a word file


  ngOnInit() {
  }

  saveFile(event) {
    this.file = event.target.files[0];
    this.filepath = this.file.name;

    this.testImage();
    this.testDoc();
       this.selectedFile = true;
    
 if(this.emailAddress!=""&&this.ticketId!=""&&this.validator.validate(this.emailAddress)
 &&  this.selectedFile== true && this.numberOfMinutes >=0  )  {
  this.yesUpload = true;
 }


    const reader = new FileReader();

    reader.onload = (e) => {
        const result: string = reader.result.toString();
        this.fileContent = result;

      if (this.isImage) {
        const imgTag = document.getElementById('image'); 
        imgTag.setAttribute('src', this.fileContent);
      } else if (this.isPdf) {
        const embedded = document.getElementById('pdf'); 
        embedded.setAttribute('src', this.fileContent);
      }
    };


    if (this.isImage || this.isPdf) {
      reader.readAsDataURL(this.file);
    } else {
      reader.readAsText(this.file);
    }

  }

  upload() {
    


  }

  testImage() {
    const imageExtensions = ['png', 'jpg', 'gif'];
    this.isImage = imageExtensions
      .map(elem => this.file.name.endsWith(elem))
      .reduce((acc, elem) => acc || elem, false);

     
  }

  testDoc() {
    const docExtensions = ['doc', 'docx', 'html', 'pptx', 'xlsx', 'pdf'];
    this.isPdf = docExtensions
      .map(elem => this.file.name.endsWith(elem))
      .reduce((acc, elem) => acc || elem, false);
  }


  

  onUpdateNumberOfMinutes(event: Event) {

this.numberOfMinutes = (<HTMLInputElement>event.target).valueAsNumber;

if(this.emailAddress!=""&&this.ticketId!=""&&this.validator.validate(this.emailAddress)
  &&  this.selectedFile== true   && this.numberOfMinutes>=0   )  {
   this.yesUpload = true;
  }

  }

    
onUpdateTicketId(event: Event) {

  this.ticketId = (<HTMLInputElement>event.target).value;
 
 
if(this.emailAddress!=""&&this.ticketId!=""&&this.validator.validate(this.emailAddress)
&&  this.selectedFile== true   && this.numberOfMinutes>=0   )  {
 this.yesUpload = true;
}

}

constructor (private httpClient: HttpClient){}

//we have to see how to do the post request properly

onUploadCV(event : Event) {


 this.httpClient.post('https://monolith.joobilix.com/api/v1/Cv/Admin/Cv', 

 {
  cvData : this.filepath,
   cvName : this.filepath,
   numberOfMinutes : this.numberOfMinutes,
   ticketId : this.ticketId
})
//.subscribe(
//  (data: any) => {
 //   console.log(data)
//  }
//)
this.filepath = "";

}

onUpdateEmailAddress(event: Event) {

  this.emailAddress = (<HTMLInputElement>event.target).value;
  
if(this.emailAddress!=""&&this.ticketId!=""&&this.validator.validate(this.emailAddress)
&&  this.selectedFile== true   && this.numberOfMinutes>=0   )  {
 this.yesUpload = true;
}

}

}